var express = require("express");

const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.json());

var server = app.listen(3306, function(){
    console.log("Node.js is listening to PORT:" + server.address().port);
});

app.set('view engine', 'ejs');

app.get("/", function(req, res, next){
    res.render('registration', {});
});

//post
app.post("/Completion", function(req, res, next)
{
    var student = {
        first : req.body.fname,
        last : req.body.lname
    }
    console.log(student);

    res.render('completion',{
        userValue : student,
        topicHead : 'Student Form'
    });

});
